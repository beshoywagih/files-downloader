package com.nagwa.downloaderapp.data.dataSource.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import com.nagwa.downloaderapp.data.dto.FileData

@Database(entities = [FileData::class], version = 1)
abstract class FilesDatabase : RoomDatabase() {
    abstract val fileDao: FileDao

    companion object {
        const val DatabaseName = "FilesDatabase"
    }
}