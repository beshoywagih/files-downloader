package com.nagwa.downloaderapp.data.repository

import com.nagwa.downloaderapp.data.dataSource.cache.FileDao
import com.nagwa.downloaderapp.data.dto.FileData
import com.nagwa.downloaderapp.domain.repos.FilesRepository
import io.reactivex.Completable
import io.reactivex.Single

class FilesRepositoryImp(private val fileDao: FileDao) : FilesRepository {

    override fun getFiles(): Single<List<FileData>> {
        return fileDao.getFiles()
    }

    override fun updateDownloadState(id: Int, isDownloaded: Boolean): Completable {
        return fileDao.updateFileDownloadState(id, isDownloaded)
    }

    override fun insertFiles(files: List<FileData>): Completable {
        return fileDao.insertAllFiles(files)
    }

    override fun insertFile(file: FileData): Completable {
        return fileDao.insertFile(file)
    }
}