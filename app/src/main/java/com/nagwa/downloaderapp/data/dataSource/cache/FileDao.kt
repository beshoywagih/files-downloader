package com.nagwa.downloaderapp.data.dataSource.cache

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single
import com.nagwa.downloaderapp.data.dto.FileData


@Dao
interface FileDao {

    @Query("SELECT * FROM FileData")
    fun getFiles(): Single<List<FileData>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertFile(data: FileData): Completable

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAllFiles(order: List<FileData?>?): Completable

    @Query("UPDATE FileData SET is_downloaded = :isDownloaded WHERE id =:id")
    fun updateFileDownloadState(id: Int, isDownloaded: Boolean?): Completable

}