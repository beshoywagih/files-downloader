package com.nagwa.downloaderapp.data.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(indices = [Index(value = ["id"], unique = true)])
data class FileData(
    @PrimaryKey(autoGenerate = true) val rowId: Int?,
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "type") val type: String,
    @ColumnInfo(name = "url") val url: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "is_downloaded") var isDownloaded: Int?,
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB) var image: ByteArray?
)