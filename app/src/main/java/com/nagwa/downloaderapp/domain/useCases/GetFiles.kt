package com.nagwa.downloaderapp.domain.useCases

import com.nagwa.downloaderapp.data.dto.FileData
import com.nagwa.downloaderapp.domain.repos.FilesRepository
import com.nagwa.downloaderapp.domain.util.FileSort
import com.nagwa.downloaderapp.enums.FileInfo
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.Exception
import javax.inject.Inject

class GetFiles @Inject constructor(private val filesRepository: FilesRepository) {
    //this method return files depended on download status
    operator fun invoke(fileType: FileSort): Single<List<FileData>> {
        return filesRepository.getFiles()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).map { it ->
                when (fileType) {
                    FileSort.All -> it
                    FileSort.Downloaded -> it.sortedBy { it.isDownloaded == FileInfo.downloaded }
                    FileSort.UnDownloaded -> it.sortedBy { it.isDownloaded == FileInfo.unDownloaded }
                    else -> throw Exception("you can't sort with this type")
                }

            }
    }
}