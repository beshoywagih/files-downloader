package com.nagwa.downloaderapp.domain.models

import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class File(
    @SerializedName("id") val id: Int,
    @SerializedName("type") val type: String,
    @SerializedName("url") val url: String,
    @SerializedName("name") val name: String,
)