package com.nagwa.downloaderapp.domain.util

sealed class FileSort {
    object All : FileSort()
    object Downloaded : FileSort()
    object UnDownloaded : FileSort()
}