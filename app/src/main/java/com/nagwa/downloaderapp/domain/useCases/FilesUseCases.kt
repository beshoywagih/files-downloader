package com.nagwa.downloaderapp.domain.useCases

import javax.inject.Inject

data class FilesUseCases @Inject constructor(
    val getFilesUseCase: GetFiles,
    val insertFilesUseCase: InsertFiles,
    val downloadUseCase: UpdateFileUseCase
)