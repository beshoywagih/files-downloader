package com.nagwa.downloaderapp.domain.util

import android.content.Context
import android.graphics.Bitmap

import android.net.Uri

import android.os.ParcelFileDescriptor
import android.util.Log
import java.lang.Exception
import android.media.MediaMetadataRetriever

import android.os.Build
import io.reactivex.android.schedulers.AndroidSchedulers

import io.reactivex.schedulers.Schedulers


class Thumbnails {
    companion object {

        fun retrieveVideoFrameFromVideo(videoPath: String?): Bitmap? {
            var bitmap: Bitmap? = null
            var mediaMetadataRetriever: MediaMetadataRetriever? = null
            try {
                mediaMetadataRetriever = MediaMetadataRetriever()
                mediaMetadataRetriever.setDataSource(videoPath, HashMap())
                bitmap = mediaMetadataRetriever.getFrameAtTime(2, MediaMetadataRetriever.OPTION_CLOSEST)
            } catch (e: Exception) {
                e.printStackTrace()
//                throw Throwable("Exception in retrieve Video" + e.message)
            } finally {
                mediaMetadataRetriever?.release()
            }
            return bitmap
        }

    }
}

