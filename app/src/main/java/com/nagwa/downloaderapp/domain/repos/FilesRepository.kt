package com.nagwa.downloaderapp.domain.repos

import com.nagwa.downloaderapp.data.dto.FileData
import io.reactivex.Completable
import io.reactivex.Single

interface FilesRepository {
    fun getFiles(): Single<List<FileData>>
    fun updateDownloadState(id: Int, isDownloaded: Boolean): Completable
    fun insertFiles(files: List<FileData>): Completable
    fun insertFile(file: FileData): Completable
}