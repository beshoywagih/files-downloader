package com.nagwa.downloaderapp.domain.useCases

import com.nagwa.downloaderapp.domain.repos.FilesRepository
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

data class UpdateFileUseCase @Inject constructor(private val filesRepository: FilesRepository) {

    fun updateBookDownloadState(bookId: Int, isDownloaded: Boolean): Completable {
        return filesRepository.updateDownloadState(bookId, isDownloaded)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}