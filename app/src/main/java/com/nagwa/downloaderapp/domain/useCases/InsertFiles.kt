package com.nagwa.downloaderapp.domain.useCases

import android.graphics.Bitmap
import android.util.Log
import com.nagwa.downloaderapp.data.dto.FileData
import com.nagwa.downloaderapp.domain.models.File
import com.nagwa.downloaderapp.domain.repos.FilesRepository
import com.nagwa.downloaderapp.domain.util.Thumbnails
import com.nagwa.downloaderapp.enums.FileInfo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.ByteArrayOutputStream
import javax.inject.Inject
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.subjects.PublishSubject


class InsertFiles @Inject constructor(private val filesRepository: FilesRepository) {
    private var disposable = CompositeDisposable()

    //convert list of files to database model and generate thumbnail then insert files to database
    fun insertFiles(files: List<File>): Observable<Boolean> {
        val convertedFiles: MutableList<FileData> = mutableListOf()
        return Observable.create {
            val filesTables = convertListToDataBaseModel(files)
            disposable.add(
                getImage(filesTables).subscribe(
                    {
                        convertedFiles.add(it)
                    },
                    {
                        Log.e("error while get files thumbnail", it.message.toString())
                    },
                    {
                        filesRepository.insertFiles(convertedFiles)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(object : DisposableCompletableObserver() {
                                override fun onComplete() {
                                    it.onNext(true)
                                    disposable.dispose()
                                }

                                override fun onError(e: Throwable) {
                                    Log.e("error while insert files", e.message.toString())
                                }

                            })
                    },
                )
            )
        }
    }

    //get thumbnail for files
    private fun getImage(identifiers: List<FileData>): Observable<FileData> {
        val publishSubject = PublishSubject.create<Int>()
        var currentIndex = 0
        return Observable.create<FileData> { emitter ->
            disposable.add(publishSubject.subscribe { i ->
                if (identifiers[i].type != FileInfo.video) {
                    emitter.onNext(identifiers[i])
                    if (i == identifiers.size - 1) {
                        emitter.onComplete()
                    } else {
                        if (i < identifiers.size)
                            publishSubject.onNext(currentIndex++)
                    }
                } else {
                    disposable.add(
                        Observable.fromCallable<Bitmap?> {
                            Thumbnails.retrieveVideoFrameFromVideo(
                                identifiers[i].url
                            )
                        }.subscribeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                { bitmap ->
                                    bitmap?.let {
                                        identifiers[i].image = getImageBytes(bitmap)
                                    }
                                    emitter.onNext(identifiers[i])
                                    if (i == identifiers.size - 1) {
                                        emitter.onComplete()
                                    } else {
                                        if (i < identifiers.size)
                                            publishSubject.onNext(currentIndex++)
                                    }
                                },
                                { error ->
                                    Log.e("thumbnail error", error.message.toString())
                                    emitter.onNext(identifiers[i])
                                    if (i == identifiers.size - 1) {
                                        emitter.onComplete()
                                    } else {
                                        if (i < identifiers.size)
                                            publishSubject.onNext(currentIndex++)
                                    }
                                },
                            )
                    )
                }
            })
            publishSubject.onNext(currentIndex)
        }.subscribeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread())
    }

    //convert bitmap to bytes
    private fun getImageBytes(image: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 90, stream)
        return stream.toByteArray()
    }

    //convert list of files to table model
    private fun convertListToDataBaseModel(files: List<File>): List<FileData> {
        return files.map { file ->
            FileData(
                id = file.id,
                name = file.name,
                type = file.type,
                url = file.url,
                isDownloaded = FileInfo.unDownloaded,
                rowId = null,
                image = null
            )
        }
    }

}
