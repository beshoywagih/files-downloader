package com.nagwa.downloaderapp.di.application

import android.app.Application
import android.content.Context
import com.nagwa.downloaderapp.FilesApp
import com.nagwa.downloaderapp.di.viewModel.ViewModelModule
import com.nagwa.downloaderapp.presenter.screen.FilesActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, ViewModelModule::class])
interface ApplicationComponent {
    fun inject(app: FilesApp?)
    fun inject(app: FilesActivity?)
    val application: Application?

    @get:ApplicationContext
    val context: Context?
}
