package com.nagwa.downloaderapp.di.application

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.room.Room
import com.nagwa.downloaderapp.data.dataSource.cache.FileDao
import com.nagwa.downloaderapp.data.dataSource.cache.FilesDatabase
import com.nagwa.downloaderapp.data.repository.FilesRepositoryImp
import com.nagwa.downloaderapp.domain.repos.FilesRepository
import com.nagwa.downloaderapp.domain.useCases.GetFiles
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
class ApplicationModule
    (private val mApplication: Application) {
    @Provides
    @ApplicationContext
    fun provideContext(): Context {
        return mApplication
    }

    @Provides
    fun provideApplication(): Application {
        return mApplication
    }


    @Singleton
    @Provides
    fun provideFilesDB(app: Application): FilesDatabase {
        return Room.databaseBuilder(app, FilesDatabase::class.java, FilesDatabase.DatabaseName)
            .build()
    }

    @Singleton
    @Provides
    fun provideFileDao(filesDatabase: FilesDatabase): FileDao {
        return filesDatabase.fileDao
    }

    @Singleton
    @Provides
    fun provideUserPrefs(fileDao: FileDao): FilesRepository {
        return FilesRepositoryImp(fileDao)
    }

    @Singleton
    @Provides
    fun provideGetFilesUseCase(filesRepository: FilesRepository): GetFiles {
        return GetFiles(filesRepository)
    }
}
