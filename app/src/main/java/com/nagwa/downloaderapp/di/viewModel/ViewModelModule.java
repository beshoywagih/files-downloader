package com.nagwa.downloaderapp.di.viewModel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.nagwa.downloaderapp.presenter.viewModel.FilesVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
abstract public class ViewModelModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(DaggerViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(FilesVM.class)
    abstract ViewModel provideBaseViewModel(FilesVM viewModel);

}
