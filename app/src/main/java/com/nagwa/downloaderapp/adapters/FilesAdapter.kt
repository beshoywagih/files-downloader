package com.nagwa.downloaderapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nagwa.downloaderapp.R
import com.nagwa.downloaderapp.base.BaseAdapter
import com.nagwa.downloaderapp.enums.FileInfo
import coil.load
import android.graphics.BitmapFactory
import coil.size.Scale
import com.nagwa.downloaderapp.data.dto.FileData


class FilesAdapter(val context: Context) :
    RecyclerView.Adapter<FilesAdapter.ViewHolder>(), BaseAdapter<FileData> {
    var onItemClick: ((FileData) -> Unit)? = null

    private val files: MutableList<FileData> = mutableListOf()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener { onItemClick?.invoke(files[adapterPosition]) }
        }

        val filetTitle: TextView = view.findViewById(R.id.file_title)
        val fileImage: ImageView = view.findViewById(R.id.file_image)
        val downloadLayer: View = view.findViewById(R.id.download_layer)
        val fileType: TextView = view.findViewById(R.id.file_type)

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.file_item, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.filetTitle.text = files[position].name
        viewHolder.fileType.text = files[position].type

        //hide download layer if file already downloaded
        if (files[position].isDownloaded == FileInfo.downloaded) {
            viewHolder.downloadLayer.visibility = View.GONE
        }


        /* if file type is image
        *   - there is thumbnail show thumbnail image
        *   - there is no thumbnail show image holder
        *  if file type is pdf
        *   - show image holder for pdf file */
        if (files[position].type == FileInfo.video) {
            files[position].image?.let {
                viewHolder.fileImage.scaleType = ImageView.ScaleType.FIT_XY
                viewHolder.fileImage.load(
                    BitmapFactory.decodeByteArray(
                        files[position].image,
                        0,
                        files[position].image!!.size
                    )
                ) {
                    crossfade(true)
                    scale(Scale.FILL)
                }
            } ?: viewHolder.fileImage.load(R.drawable.error) {
                crossfade(true)
            }
        } else {
            viewHolder.fileImage.load(R.drawable.pdf) {
                crossfade(true)
            }
        }
    }

    override fun getItemCount() = files.size

    override fun addAll(items: List<FileData>?) {
        files.addAll(items!!.toMutableList())
        notifyDataSetChanged()
    }

    fun updateFile(file: FileData) {
        val index = files.indexOf(file)
        files[index] = file
        notifyItemChanged(index)
    }
}



