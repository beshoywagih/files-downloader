package com.nagwa.downloaderapp.base

interface BaseView {
    fun showLoading()
    fun hideLoading()
}