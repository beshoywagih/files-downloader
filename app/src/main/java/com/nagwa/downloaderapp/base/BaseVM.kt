package com.nagwa.downloaderapp.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseVM : ViewModel() {
    protected var disposable = CompositeDisposable()

}