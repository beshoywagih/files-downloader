package com.nagwa.downloaderapp.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.viewbinding.ViewBinding
import cc.cloudist.acplibrary.ACProgressFlower
import com.nagwa.downloaderapp.FilesApp
import com.nagwa.downloaderapp.R
import com.nagwa.downloaderapp.di.application.ApplicationComponent
import com.nagwa.downloaderapp.di.viewModel.DaggerViewModelFactory
import javax.inject.Inject


abstract class BaseActivity<V : ViewBinding, T : BaseVM> : AppCompatActivity(), BaseView {
    val showLoadingMore: LiveData<Boolean> = MutableLiveData()
    private lateinit var binding: V
    lateinit var viewModel: T
    lateinit var dialog: ACProgressFlower
    lateinit var daggerComponent: ApplicationComponent

    @Inject
    lateinit var viewModelFactory: DaggerViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = initBindingView()
        setContentView(binding.root)
        daggerComponent = (application as FilesApp).applicationComponent
        inject()
        initViewModel()?.let { viewModel = initViewModel()!! }
    }

    override fun onStart() {
        super.onStart()
        onCreateView()
    }

    abstract fun onCreateView()

    abstract fun initBindingView(): V

    abstract fun initViewModel(): T?

    protected abstract fun inject()

    override fun showLoading() {
        dialog.show()
    }

    override fun hideLoading() {
        dialog.dismiss()
    }

}