package com.nagwa.downloaderapp.base

interface BaseAdapter<T> {
    fun addAll(items: List<T>?)
}