package com.nagwa.downloaderapp

import android.app.Application
import androidx.room.Room
import com.nagwa.downloaderapp.di.application.ApplicationComponent
import com.nagwa.downloaderapp.di.application.ApplicationModule
import com.nagwa.downloaderapp.di.application.DaggerApplicationComponent
import kotlin.reflect.KProperty

class FilesApp : Application() {
    lateinit var applicationComponent: ApplicationComponent;
    override fun onCreate() {
        super.onCreate()
        initializeDaggerComponent()
    }

    private fun initializeDaggerComponent() {
        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
        applicationComponent.inject(this)
    }
}