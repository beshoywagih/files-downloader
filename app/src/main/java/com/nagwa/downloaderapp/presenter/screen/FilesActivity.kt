package com.nagwa.downloaderapp.presenter.screen

import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.nagwa.downloaderapp.base.BaseActivity
import com.nagwa.downloaderapp.databinding.ActivityMainBinding
import com.nagwa.downloaderapp.presenter.viewModel.FilesVM
import javax.inject.Inject

import androidx.recyclerview.widget.GridLayoutManager
import com.nagwa.downloaderapp.adapters.FilesAdapter
import com.nagwa.downloaderapp.enums.FileInfo
import com.nagwa.downloaderapp.presenter.dialogs.DownloadDialog
import io.reactivex.observers.DisposableCompletableObserver
import kotlinx.android.synthetic.main.activity_main.*


class FilesActivity : BaseActivity<ActivityMainBinding, FilesVM>() {
    @Inject
    lateinit var registrationViewModel: FilesVM

    @Inject
    lateinit var downloadDialog: DownloadDialog

    lateinit var filesAdapter: FilesAdapter

    override fun onCreateView() {

        // init recyclerview
        initFilesRec()

        // observe on database
        viewModel.getFiles(this).observe(this, {
            loadingProgress.visibility = View.GONE
            filesGrid.visibility = View.VISIBLE
            it?.let {
                filesAdapter.addAll(it)
            }
        })
    }


    override fun initBindingView(): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }

    override fun initViewModel(): FilesVM {
        return ViewModelProvider(this, viewModelFactory)[FilesVM::class.java]
    }

    override fun inject() {
        daggerComponent.inject(this)
    }

    private fun initFilesRec() {

        //create adapter and set it to recyclerView
        filesAdapter = FilesAdapter(this)
        val layoutManager = GridLayoutManager(this, 2)
        filesGrid.layoutManager = layoutManager
        filesGrid.adapter = filesAdapter

        //on item click
        filesAdapter.onItemClick = {
            if (it.isDownloaded == FileInfo.unDownloaded)
                downloadDialog.showDialog(this) {
                    viewModel.changeBookDownloadState(it.id, true)
                        .subscribe(object : DisposableCompletableObserver() {
                            override fun onComplete() {
                                it.isDownloaded = FileInfo.downloaded
                                filesAdapter.updateFile(it)
                            }

                            override fun onError(e: Throwable) {
                                Log.e("TAG", "Error" + e.localizedMessage)
                            }
                        })
                }
        }
    }


}


