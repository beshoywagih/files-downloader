package com.nagwa.downloaderapp.presenter.dialogs

import android.app.Activity
import android.app.Dialog
import android.view.Window
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.TextView
import com.nagwa.downloaderapp.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class DownloadDialog @Inject constructor() {
    lateinit var disposable: Disposable
    private var randomDownloadSpeed: Int = 1000
    private var currentProgress: Int = 0

    fun showDialog(
        activity: Activity, onDownloadFinished: () -> Unit
    ) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog)
        val progressBar: ProgressBar = dialog.findViewById(R.id.progress_horizontal) as ProgressBar
        val progressPercentage: TextView = dialog.findViewById(R.id.progress_percentage)
        val newInterval = PublishSubject.create<Long>()

        disposable = newInterval.switchMap { currentPeriod: Long? ->
            Observable.interval(
                currentPeriod!!, TimeUnit.MILLISECONDS
            )
        }
            .doOnNext {
                currentProgress++
                activity.runOnUiThread {
                    progressBar.progress = currentProgress
                    progressPercentage.text = currentProgress.toString()
                }
                if (currentProgress >= 101) {
                    finishDownload()
                    dialog.dismiss()
                    onDownloadFinished.invoke()
                }
                randomDownloadSpeed = generateRandomSpeed()
                newInterval.onNext(randomDownloadSpeed.toLong())
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
        newInterval.onNext(randomDownloadSpeed.toLong())
        dialog.show()
        val window: Window = dialog.window!!
        window.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    //reset progress and dispose PublishSubject listener
    private fun finishDownload() {
        disposable.dispose()
        currentProgress = 0
    }

    //it is generate random network speed to give user real network speed frequency feeling
    private fun generateRandomSpeed(): Int {
        return (0..10).random() * 30
    }
}