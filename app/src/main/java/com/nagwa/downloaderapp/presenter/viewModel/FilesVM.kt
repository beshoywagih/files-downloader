package com.nagwa.downloaderapp.presenter.viewModel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nagwa.downloaderapp.AppConstant
import com.nagwa.downloaderapp.base.BaseVM
import com.nagwa.downloaderapp.data.dto.FileData
import com.nagwa.downloaderapp.domain.models.File
import com.nagwa.downloaderapp.domain.useCases.FilesUseCases
import com.nagwa.downloaderapp.domain.util.FileSort
import io.reactivex.Completable
import io.reactivex.observers.DisposableCompletableObserver
import java.io.IOException
import javax.inject.Inject


class FilesVM @Inject constructor(private val filesUseCases: FilesUseCases) : BaseVM() {
    private val filesLiveData = MutableLiveData<List<FileData>?>()
    lateinit var context: Context

    fun getFiles(context: Context): MutableLiveData<List<FileData>?> {
        this.context = context
        getAllFiles()
        return filesLiveData
    }

    // get files form database
    private fun getAllFiles() {
        val singleFiles = filesUseCases.getFilesUseCase.invoke(FileSort.All)
        disposable.add(
            singleFiles
                .subscribe(
                    { files ->
                        if (files.isNotEmpty()) {
                            filesLiveData.postValue(files)
                            disposable.dispose()
                        } else {
                            updateBooks(context)
                        }
                    },
                    { error -> println("Error: $error") },
                )
        )
    }

    // insert files to database then call getAllFiles method to update view with last data in database
    private fun insertFiles(files: List<File>) {
        disposable.add(
            filesUseCases.insertFilesUseCase.insertFiles(files)
                .subscribe(
                    {
                        getAllFiles()
                    },
                    { error ->
                        Log.e("file insertion error", error.message.toString())
                        getAllFiles()
                    },
                )
        )
    }

    // update download file status
    fun changeBookDownloadState(bookId: Int, isDownloaded: Boolean): Completable {
        return filesUseCases.downloadUseCase.updateBookDownloadState(bookId, isDownloaded)
    }

    // read json file and parsing to file model then insert files to database
    fun updateBooks(context: Context) {
        val filesData: String
        try {
            filesData =
                context.assets.open(AppConstant.fileName).bufferedReader().use { it.readText() }
            val gson = Gson()
            val listPersonType = object : TypeToken<List<File>>() {}.type
            val files: List<File> = gson.fromJson(filesData, listPersonType)
            insertFiles(files)
        } catch (ioException: IOException) {
            ioException.printStackTrace()
        }
    }
}
