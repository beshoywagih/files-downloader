package com.nagwa.downloaderapp.enums

class FileInfo
{
    companion object {
        const val pdf = "PDF"
        const val video = "VIDEO"
        const val downloaded = 1
        const val unDownloaded = 0
    }
}